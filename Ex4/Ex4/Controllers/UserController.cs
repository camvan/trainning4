﻿using System.Data.Common;
using System.Threading.Tasks;
using Ex4.Models;
using Ex4.UserService;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;

namespace Ex4.Controllers
{
    public class UserController : Controller
    {
        
        private readonly IUser _userRepository;

        public UserController(IUser userRepository)
        {
            _userRepository = userRepository;
        }
        public IActionResult Index()
        {
       return View(_userRepository.GetUsers());
        }
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(User user)
        {
            if (ModelState.IsValid)
            {
                _userRepository.InsertUser(user);
                _userRepository.Save();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Create");
        }

        /*[HttpGet]
        public IActionResult Delete(int? id)
        {
            return View(_userRepository.GetUserByID(id));
        }
*/

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            if (id==null)
            {
                /*return BadRequest();*/
                return NotFound();
            }
            _userRepository.DeleteUser(id);
           
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            return View(_userRepository.GetUserByID(id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(User users)
        {
            /*if (ModelState.IsValid)*/
            /*{*/
                _userRepository.UpdateUser(users);
              
                return RedirectToAction("Index");
            /*}*/

            /*return RedirectToAction("Index");*/
        }
    }
}