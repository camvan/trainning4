﻿using System;
using System.Collections.Generic;
using Ex4.Models;

namespace Ex4.UserService
{
    public interface IUser :IDisposable
    {
        IEnumerable<User> GetUsers();
        User GetUserByID(int? Id);
        void InsertUser(User user);
        void DeleteUser(int Id);
        void UpdateUser(User user);
        void Save();
    }
}