﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ex4.Models;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.EntityFrameworkCore;

namespace Ex4.UserService
{
    public class UserRepository : IUser , IDisposable
    {
        private readonly UserDbContext _userRepository;
       
 
        public UserRepository(UserDbContext context)
        {
            this._userRepository = context;
        }
 
        public IEnumerable<User> GetUsers()
        {
            return _userRepository.Users;
        }
 
        public User GetUserByID(int? id)
        {
            return _userRepository.Users.Find(id);
        }

        public void InsertUser(User user)
        {
            _userRepository.Users.Add(user);
        }

    
 
        public void DeleteUser(int Id)
        {
            User user= _userRepository.Users.Find(Id);
            _userRepository.Users.Remove(user);
            _userRepository.SaveChanges();
        }
 
        public void UpdateUser(User user)
        {
            _userRepository.Entry(user).State = EntityState.Modified;
            _userRepository.SaveChanges();
        }
 
        public void Save()
        {
            _userRepository.SaveChanges();
        }


       
        private bool disposed = false;
 
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _userRepository.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}