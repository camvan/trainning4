﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Ex4.Models
{
    public class UserDbContext : DbContext
    {
        public UserDbContext(DbContextOptions options) : base(options)  
        {  
        }

        public DbSet<User> Users { set; get; }
    }
}